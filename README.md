# NODEJS-EXAMPLE

This project has simple node.js code which will demostrate REST API. It will expose `/version` http endpoint to display its current version and `/healthz` to desplay health. I have packaged this as a docker image and will be pushing it at my docker registry [here](https://cloud.docker.com/repository/registry-1.docker.io/vaibhavpbits/nodejs-example). 

### Developer Setup
Follow the below steps to build and test this project locally:
* Clone this respository to local machine.
* Make sure that `node.js` and `docker` are installed on your machine. 
* Build Dockerfile by running below command.
> docker build -t username/nodejs-example .
* Verify docker image. 
> docker images
* Test.
> docker run -p 49560:8080 -d username/nodejs-example  
> curl http://localhost:49560/version

### Developer Deployment 
Follow the below steps to deploy this application on minikube:
* Start the minikube on local machine (it will take couple of minutes).
> minikube start
* Deploy manifest files from `deploy` folder.
> kubectl apply -f deploy/ -n nodejs-dev
* Verify the state of pods. 
> kubectl get pods -n nodejs-dev
* Get service port
> kubectl get svc nodejs-example -n nodejs-dev
* Curl http endpoint, it should return `0.0.1` version
> curl http://minikube-ip:nodeport/version

### Rolling Update 
Rolling update is default updates for Kubernete. It slowly, one by one, replace pods of previous version of application with pods of new version without any downtime. Rolling update waits for new pods to beacome ready before scaling down the old pods. 

* Update the `deployment.yaml` file from `deploy` folder to update docker image version to `0.0.2`.
* Apply those change in Kubernetes.
> kubectl apply -f deploy/
* One of the pre-requisite for this is application/manifest should have rediness probe configure. If it is in placed, you can see that, new pods are getting created and old one is getting terminated after they are ready.
* We can further refine it by adjusting the `MaxSurge` and `maxUnavailable` parameters in the manifest files. 

### Blue Green Deployment 
In this deployment we creates new set of deployments called `Green` which is based on new version of `nodejs-example` app, Blue deployment is the current deployment (version).

As we already have `nodejs-example` deployed in minikube, follow the below steps for upgrading it to `0.0.2.`
* Update manifest files to change image version to `0.0.2`, its deployment and service name, and lable `app`. 
* Apply these updated files. 
* List the services, now you will see two services. 
> kubectl get svc -n nodejs-dev
* Access Green deployment of `nodejs-example`.
* If everything working as expected patch actual services.
* Delete Blue deployment (excluding service).
* Delete service which is created with Green deployment for testing. 

## Production Setup
Following points should be consider to deploy this application on production environment. 
* Write pipeline to build and deploy this application (build part is already done, please check .gitlab-ci.yml).
* Create Helm charts, which will give ease to deployment. 
* Currently, we are using service type as a `NodePort`, for production, we should use `ClusterIP` with Ingress object. `NodePort` ocupies the port on all nodes of cluster. 
* Add pod disruption budget to keep application highly available. 
* If you want to go with Canary deployment for your applications, Istio goes well with that. 
