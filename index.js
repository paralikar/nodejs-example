'use strict'
const express = require("express");
const app = express();

var packvar = require('./package.json');

app.get("/version", (req, res) => {
  //res.send({hello: "world" });
  res.send(packvar.version)
});

app.get("/healthz", (req, res) => {
  res.send("ok")
});

const PORT = 8080;
app.listen(PORT, function() {
  console.log(`App listening on port ${PORT}`);
});

